<?php

namespace Drupal\restricted_layout\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;

/**
 * Restricted Layout Class.
 */
class RestrictedLayout extends LayoutDefault implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'roles' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $options = [];
    $roles = Role::loadMultiple();
    foreach ($roles as $role) {
      $options[$role->id()] = $role->label();
    }

    $configuration = $this->getConfiguration();
    $form['roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Visible for'),
      '#required' => TRUE,
      '#default_value' => $configuration['roles'],
      '#options' => $options,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $regions) {
    $uid = \Drupal::currentUser()->id();
    $user = User::load($uid);
    $roles = $user->getRoles();
    $configuration = $this->getConfiguration();

    $route_name = \Drupal::routeMatch()->getRouteName();

    $isAdminRoute = \Drupal::service('router.admin_context')->isAdminRoute();
    $isLayoutParagraphBuilderRoute = strpos($route_name, 'layout_paragraphs.builder.') === 0;
    $access = $isAdminRoute || $isLayoutParagraphBuilderRoute ? TRUE : FALSE;

    foreach ($roles as $role) {
      if ($access) {
        continue;
      }
      if (isset($configuration['roles'][$role]) && $configuration['roles'][$role] !== 0) {
        $access = TRUE;
      }
    }

    $regions['main']['#access'] = $access;

    $build = parent::build($regions);
    $build['#cache'] = ['contexts' => ['user']];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['roles'] = $form_state->getValue('roles');
  }

}
