Module: Restricted Layout
Author: David Fröhlich


Description
===========
Adds a "Restricted" layout type to your website, that allows you to
show it's contents only to certain users.


Installation
============
Copy the 'restricted_layout' module directory in to your Drupal 'modules'
directory as usual.


Usage
=====
When using the Layout Builder, select the "Restricted" layout type.
Next, use the "Layout Options" to specify access permissions.
